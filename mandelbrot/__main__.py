#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import sys
import numpy
from argparse import ArgumentParser
from mandelbrot.impl import get_mandelbrot_class, Precision
from PyQt5 import QtWidgets, QtCore, QtGui


class MandelbrotGUI(QtWidgets.QMainWindow):

    def __init__(self, mandelbrot_cls, x=0.0, y=0.0, zoom=1.0, width=800, height=600,
                 device=0, precision=Precision.DOUBLE):
        self.__width = width
        self.__height = height
        self.__x = x
        self.__y = y
        self.__zoom = zoom
        self.__max_iterations = 100
        self.__ratio = float(self.__height) / float(self.__width)
        self.__mandelbrot_cls = mandelbrot_cls
        self.__device = device
        self.__mandelbrot = mandelbrot_cls(width=self.__width, height=self.__height, device=device, precision=precision)

        # The main window
        super(MandelbrotGUI, self).__init__()
        self.setGeometry(width, height, 400, 300)
        self.setWindowTitle("Mandelbrot")

        # The image to display
        self.__image = None
        self.__pixelBuffer = None
        self.__resize_image()
        self.__imageLabel = QtWidgets.QLabel(self)
        self.setCentralWidget(self.__imageLabel)

    def __calculate_max_iterations(self):
        if self.__zoom > 50000:
            self.__max_iterations = 10000
        elif self.__zoom > 10000:
            self.__max_iterations = 5000
        elif self.__zoom > 5000:
            self.__max_iterations = 2000
        elif self.__zoom > 1000:
            self.__max_iterations = 1000
        elif self.__zoom > 100:
            self.__max_iterations = 500
        else:
            self.__max_iterations = 100

    def __calculate_new_center(self, mouse_x, mouse_y):
        if 0 > mouse_x or self.__width <= mouse_x or 0 > mouse_y or self.__height <= mouse_y:
            return
        # Calculate the minimal and maximal coordinates
        min_x, max_x = self.__x - (1 / self.__zoom), self.__x + (1 / self.__zoom)
        min_y, max_y = self.__y - (self.__ratio / self.__zoom), self.__y + (self.__ratio / self.__zoom)
        # And now calculate the new center
        self.__x = min_x + (max_x - min_x) * (mouse_x / float(self.__width))
        self.__y = min_y + (max_y - min_y) * (mouse_y / float(self.__height))

    def __draw_image(self):
        # Calculate the minimal and maximal coordinates
        self.__calculate_max_iterations()
        min_x, max_x = self.__x - (1 / self.__zoom), self.__x + (1 / self.__zoom)
        min_y, max_y = self.__y - (self.__ratio / self.__zoom), self.__y + (self.__ratio / self.__zoom)
        # Create the pixel output
        try:
            # Calculate the mandelbrot
            self.__mandelbrot.calculateWithColor(min_x, max_x, min_y, max_y, self.__max_iterations, self.__pixelBuffer)
            self.__imageLabel.setPixmap(QtGui.QPixmap.fromImage(self.__image))
        except Exception as e:
            QtWidgets.QErrorMessage().showMessage(str(e))
            del self.__mandelbrot
            QtCore.QCoreApplication.instance().quit()

    def __resize_image(self):
        self.__image = QtGui.QImage(self.__width, self.__height, QtGui.QImage.Format_RGB888)
        img_buffer = self.__image.bits()
        img_buffer.setsize(self.__image.byteCount())
        # noinspection PyArgumentList
        self.__pixelBuffer = numpy.ndarray(shape=(self.__height, self.__width, 3), dtype=numpy.uint8, buffer=img_buffer)

    # noinspection PyPep8Naming
    def resizeEvent(self, event):
        new_size = event.size()
        self.__width, self.__height = new_size.width(), new_size.height()
        self.__image = QtGui.QImage(self.__width, self.__height, QtGui.QImage.Format_RGB888)
        imgBuffer = self.__image.bits()
        imgBuffer.setsize(self.__image.byteCount())
        # noinspection PyArgumentList
        self.__pixelBuffer = numpy.ndarray(shape=(self.__height, self.__width, 3), dtype=numpy.uint8, buffer=imgBuffer)
        self.__mandelbrot.resize(self.__width, self.__height)
        self.__draw_image()
        super(MandelbrotGUI, self).resizeEvent(event)

    # noinspection PyPep8Naming
    def mouseReleaseEvent(self, event):
        # Get the click location
        self.__calculate_new_center(event.x(), event.y())
        # Redraw
        self.__draw_image()
        event.accept()

    # noinspection PyPep8Naming
    def wheelEvent(self, event):
        # The angleDelta is given in 8th of degrees (e.g 15 Degree: 15 * 8 = 120)
        degrees = event.angleDelta().y() / 8.0
        # every 15 degrees, the zoom is doubled
        if degrees > 0:
            self.__zoom *= 2.0 / 15.0 * degrees
        else:
            self.__zoom /= -2.0 / 15.0 * degrees
        self.__calculate_new_center(event.x(), event.y())
        self.__draw_image()
        event.accept()

    def show(self):
        # Resize the window
        # This will issue a redraw of the mandelbrot image
        self.resize(self.__width, self.__height)
        super(MandelbrotGUI, self).show()


def create_parser():
    parser = ArgumentParser()
    parser.add_argument("-x", default=0, type=float, help="The X coordinate of the center of the image")
    parser.add_argument("-y", default=0, type=float, help="The Y coordinate of the center of the image")
    parser.add_argument("-z", "--zoom", default=1, type=float, help="The zoom factor to use")
    parser.add_argument("-m", "--mode", default="multicore", type=str,
                        choices=["serial", "multicore", "cuda", "opencl"],
                        help="The calculation mode to use for the mandelbrot set calculations")
    parser.add_argument("-d", "--device", type=int, default=0, help="The device to use for the calculations")
    parser.add_argument("-p", "--precision", default="double", type=str, choices=["single", "double"],
                        help="The precision to use for the computation. "
                             "Please Note: Not all precisions might be available on all platforms")
    parser.add_argument("--width", default=800, type=int, help="The width of the window")
    parser.add_argument("--height", default=600, type=int, help="The height of the window")
    parser.add_argument("--list", default=False, action="store_true",
                        help="List the possible devices for the given mode (only valid for mode cuda and opencl)")
    return parser


def main():
    # Create the GUI
    parser = create_parser()
    arguments = parser.parse_args()
    mandelbrot_cls = get_mandelbrot_class(arguments.mode)
    if not arguments.list:
        app = QtWidgets.QApplication(sys.argv)
        window = MandelbrotGUI(mandelbrot_cls=mandelbrot_cls, x=arguments.x, y=arguments.y, zoom=arguments.zoom,
                               width=arguments.width, height=arguments.height, device=arguments.device,
                               precision=Precision.from_string(arguments.precision))
        window.show()
        return app.exec_()

    else:
        mandelbrot_cls.list_devices()


if __name__ == "__main__":
    sys.exit(main())
