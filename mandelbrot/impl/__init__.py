#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import abc
import numpy
from enum import Enum


class Mode(Enum):
    SERIAL = 0
    MULTICORE = 1
    CUDA = 2
    OPENCL = 3

    @classmethod
    def from_string(cls, string):
        check = string.lower()
        if check == "serial":
            return cls.SERIAL
        elif check == "multicore":
            return cls.MULTICORE
        elif check == "cuda":
            return cls.CUDA
        elif check == "opencl":
            return cls.OPENCL
        else:
            return None


class Precision(Enum):
    SINGLE = numpy.float32
    DOUBLE = numpy.float64

    @classmethod
    def from_string(cls, string):
        check = string.lower()
        if check == "single":
            return Precision.SINGLE
        elif check == "double":
            return Precision.DOUBLE
        else:
            return None

    def as_ctype(self):
        if self == Precision.SINGLE:
            return "float"
        elif self == Precision.DOUBLE:
            return "double"
        else:
            return None


class MandelbrotBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, width, height, device=0, precision=Precision.DOUBLE):
        self._width = width
        self._height = height
        self._device = device
        self._precision = precision.value

    @abc.abstractmethod
    def calculate(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        """
        Calculates the mandelbrot set between the given minimum and maximum coordinates.

        It uses at max `maxIterations` to determine whether a point is in the set or not.
        The result is stored in the given `buffer`. The buffer needs to be an array
        of shape NxM with a dtype that is compatible to numpy.uint.
        :param min_x: The minimum X coordinate to calculate the set for.
        :type min_x: float
        :param max_x: The maximum X coordinate to calculate the set for.
        :type max_x: float
        :param min_y: The minimum Y coordinate to calculate the set for.
        :type min_y: float
        :param max_y: The maximum Y coordinate to calculate the set for.
        :type max_y: float
        :param maxIterations: The maximal number of iterations to do for each point
        :type maxIterations: int
        :param output_buffer: The buffer to store the result to.
        :type output_buffer: numpy.ndarray
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def calculateWithColor(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        """
        Calculates the mandelbrot set between the given minimum and maximum coordinates.

        It uses at max `maxIterations` to determine whether a point is in the set or not.
        The result is stored in the given `buffer`.
        In contrast to `calculate`, this method does also colorize the set.
        Thus, the buffer needs to be an array of shape NxMx3 with a data type that is
        compatible to numpy.uint8.
        :param min_x: The minimum X coordinate to calculate the set for.
        :type min_x: float
        :param max_x: The maximum X coordinate to calculate the set for.
        :type max_x: float
        :param min_y: The minimum Y coordinate to calculate the set for.
        :type min_y: float
        :param max_y: The maximum Y coordinate to calculate the set for.
        :type max_y: float
        :param maxIterations: The maximal number of iterations to do for each point
        :type maxIterations: int
        :param output_buffer: The buffer to store the result to.
        :type output_buffer: numpy.ndarray
        """
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def list_devices(cls):
        raise NotImplementedError()

    def resize(self, width, height):
        self._width = width
        self._height = height


def get_mandelbrot_class(mode):
    if not isinstance(mode, Mode):
        mode = Mode.from_string(mode)

    if mode == Mode.SERIAL:
        from mandelbrot.impl.serial import Mandelbrot
        return Mandelbrot
    elif mode == Mode.MULTICORE:
        from mandelbrot.impl.multicore import Mandelbrot
        return Mandelbrot
    elif mode == Mode.CUDA:
        from mandelbrot.impl.cuda import Mandelbrot
        return Mandelbrot
    elif mode == Mode.OPENCL:
        from mandelbrot.impl.opencl import Mandelbrot
        return Mandelbrot
