#include <math.h>

extern "C" {

    __device__ unsigned calcMandelbrotPoint(const double c_r, const double c_i, const unsigned maxIterations)
    {
        double z_r = c_r;
        double z_i = c_i;
        double z_rsqr = z_r * z_r;
        double z_isqr = z_i * z_i;
        // Fast exit
        if(z_rsqr + z_isqr > 4.0) return 0;

        for(unsigned i = 1; i < maxIterations; i++) {
            z_i = z_r * z_i;
            z_i += z_i;
            z_i += c_i;
            z_r = z_rsqr - z_isqr + c_r;
            z_rsqr = z_r * z_r;
            z_isqr = z_i * z_i;
            if(z_rsqr + z_isqr > 4.0) return i;
        }
        return maxIterations;
    }

    __device__ unsigned idx2d(const int x, const int y, const int strideX, const int strideY)
    {
        return x * strideX + y * strideY;
    }

    __global__ void mandelbrot(const double min_x, const double max_x, const double min_y, const double max_y,
                               const unsigned maxIterations, unsigned *buffer, const int strideX, const int strideY)
    {
        const int height = blockDim.x * gridDim.x;
        const int width = blockDim.y * gridDim.y;
        const int i = threadIdx.x + blockDim.x * blockIdx.x;
        const int j = threadIdx.y + blockDim.y * blockIdx.y;
        const unsigned id = idx2d(i, j, strideX, strideY);

        const double deltaX = (max_x - min_x) / width;
        const double deltaY = (max_y - min_y) / height;
        const double c_r = min_x + deltaX * j;
        const double c_i = min_y + deltaY * i;

        buffer[id] = calcMandelbrotPoint(c_r, c_i, maxIterations);
    }

    __global__ void colorMandelbrot(const unsigned maxIterations,
                                    unsigned *source, const int src_strideX, const int src_strideY,
                                    unsigned char *buffer, const int dest_strideX, const int dest_strideY) {
        const int i = threadIdx.x + blockDim.x * blockIdx.x;
        const int j = threadIdx.y + blockDim.y * blockIdx.y;
        const unsigned src_id = idx2d(i, j, src_strideX, src_strideY);
        const unsigned dest_id = idx2d(i, j, dest_strideX, dest_strideY);
        const unsigned iterations = source[src_id];

        unsigned char r, g, b;
        if(iterations == maxIterations) {
            r = 0;
            g = 0;
            b = 0;
        } else {
            unsigned number_of_colors = ceil(powf(maxIterations, 1/3.));
            if(number_of_colors > 256) {
                number_of_colors = 256;
            }
            unsigned noc_pow = number_of_colors * number_of_colors;
            const unsigned color_steps = 256 / number_of_colors;
            if(iterations < number_of_colors) {
                b = iterations * color_steps;
                g = 0;
                r = 0;
            } else if(iterations < noc_pow) {
                b = 255;
                g = (iterations % number_of_colors) * color_steps;
                r = 0;
            } else {
                b = 255;
                g = 255;
                r = (iterations % number_of_colors) * color_steps;
            }
        }
        buffer[dest_id + 0] = r;
        buffer[dest_id + 1] = g;
        buffer[dest_id + 2] = b;
    }
}
