#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
from . import MandelbrotBase
import numpy
import math


class Mandelbrot(MandelbrotBase):

    @staticmethod
    def __iterMandelbrot(c, max_iterations, z):
        output = numpy.zeros(c.shape, dtype=numpy.uint)
        for i in range(max_iterations):
            z = z * z + c
            done = numpy.greater(abs(z), 2.0)
            c = numpy.where(done, 0+0j, c)
            z = numpy.where(done, 0+0j, z)
            output = numpy.where(done, i, output)
        return output

    @staticmethod
    def __colorMandelbrot(max_iterations, input_buffer):
        output = numpy.zeros(shape=(input_buffer.size, 3), dtype=numpy.uint8)
        for id in range(len(input_buffer)):
            iterations = input_buffer[id]
            if iterations == max_iterations:
                r, g, b = 0, 0, 0
            else:
                number_of_colors = math.ceil(math.pow(max_iterations, 1/3.))
                if number_of_colors > 256:
                    number_of_colors = 256
                color_steps = 256 / number_of_colors
                if iterations < number_of_colors:
                    r, g, b = 0, 0, iterations * color_steps
                elif iterations < math.pow(number_of_colors, 2):
                    r, g, b = 0, (iterations % number_of_colors) * color_steps, 255
                else:
                    r, g, b = (iterations % number_of_colors) * color_steps, 255, 255
            output[id] = [r, g, b]
        return output

    @staticmethod
    def list_devices():
        print("0: The local CPU")

    def calculate(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        c_r = numpy.linspace(start=min_x, stop=max_x, num=self._width, dtype=self._precision)
        c_i = numpy.linspace(start=min_y, stop=max_y, num=self._height, dtype=self._precision)

        c = numpy.ravel(c_r +  1j * c_i[:, numpy.newaxis])
        z = numpy.zeros(shape=c.shape, dtype=numpy.uint)
        output_buffer[...] = self.__iterMandelbrot(c, max_iterations, z).reshape(output_buffer.shape)

    def calculateWithColor(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        tmp_buffer = numpy.ndarray(shape=(self._width * self._height), dtype=numpy.uint)
        self.calculate(min_x, max_x, min_y, max_y, max_iterations, tmp_buffer)
        output = self.__colorMandelbrot(max_iterations, tmp_buffer)
        output_buffer[...] = output.reshape(output_buffer.shape).astype(output_buffer.dtype)
