#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import numpy
import math
import os
from fractions import gcd
from pycuda import driver, gpuarray, autoinit
from pycuda.compiler import SourceModule
from . import MandelbrotBase, Precision


FILE_DIR = os.path.abspath(os.path.dirname(__file__))


class Mandelbrot(MandelbrotBase):

    def __init__(self, width, height, device=0, precision=Precision.DOUBLE):
        super(Mandelbrot, self).__init__(width, height, device=device, precision=precision)
        # Initialize the CUDA device
        driver.init()
        try:
            device = driver.Device(device)
            # noinspection PyArgumentList
            self.__ctx = device.make_context()
        except driver.LogicError:
            self.__ctx = None
            raise

        # noinspection PyArgumentList
        if device.compute_capability()[0] < 2 and Precision(self._precision) == Precision.DOUBLE:
            raise ValueError("Double precision computation is not available for CUDA Version > 2.0")
        with open(os.path.join(FILE_DIR, "kernel.cu"), "rb") as kernel:
            source = kernel.read().decode("UTF-8")
            source = source.replace("double", Precision(self._precision).as_ctype())
            mod = SourceModule(source)
        self.KERNEL = mod.get_function("mandelbrot")
        self.COLOR_KERNEL = mod.get_function("colorMandelbrot")
        self.resize(width, height)

    def __del__(self):
        if self.__ctx is not None:
            self.__ctx.pop()
            self.__ctx = None
            from pycuda.tools import clear_context_caches
            clear_context_caches()

    @classmethod
    def __get_devices(cls):
        # noinspection PyArgumentList
        return [driver.Device(i) for i in range(driver.Device.count())]

    @classmethod
    def list_devices(cls):
        for i, device in enumerate(cls.__get_devices()):
            # noinspection PyArgumentList
            print("%d: %s" % (i, device.name()))

    def calculate(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        dest_stride_x = numpy.int32(output_buffer.strides[0] / output_buffer.dtype.itemsize)
        dest_stride_y = numpy.int32(output_buffer.strides[1] / output_buffer.dtype.itemsize)

        max_threads_per_block = self.KERNEL.max_threads_per_block
        thread_root = math.sqrt(max_threads_per_block)
        block_x = gcd(output_buffer.shape[0], max_threads_per_block)
        if block_x < thread_root:
            block_y = gcd(output_buffer.shape[1], max_threads_per_block)
        else:
            block_y = max_threads_per_block / block_x
        block = (block_x, block_y, 1)

        number_of_blocks = (int(output_buffer.shape[0] / block_x),
                            int(output_buffer.shape[1] / block_y), 1)
        self.KERNEL(self._precision(min_x), self._precision(max_x), self._precision(min_y), self._precision(max_y),
                    numpy.uint(max_iterations), driver.Out(output_buffer), dest_stride_x, dest_stride_y,
                    block=block, grid=number_of_blocks)

    def calculateWithColor(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        src_buffer = gpuarray.GPUArray(shape=output_buffer.shape, dtype=numpy.uint, order="F")
        src_stride_x = numpy.int32(src_buffer.strides[0] / src_buffer.dtype.itemsize)
        src_stride_y = numpy.int32(src_buffer.strides[1] / src_buffer.dtype.itemsize)
        dest_stride_x = numpy.int32(output_buffer.strides[0] / output_buffer.dtype.itemsize)
        dest_stride_y = numpy.int32(output_buffer.strides[1] / output_buffer.dtype.itemsize)

        max_threads_per_block = self.KERNEL.max_threads_per_block
        thread_root = math.sqrt(max_threads_per_block)
        block_x = gcd(output_buffer.shape[0], max_threads_per_block)
        if block_x < thread_root:
            block_y = gcd(output_buffer.shape[1], max_threads_per_block)
        else:
            block_y = max_threads_per_block / block_x
        block = (block_x, block_y, 1)

        number_of_blocks = (int(output_buffer.shape[0] / block_x),
                            int(output_buffer.shape[1] / block_y), 1)

        # Calculate the mandelbrot set, but don't copy the results from the device
        self.KERNEL(self._precision(min_x), self._precision(max_x), self._precision(min_y), self._precision(max_y),
                    numpy.uint(max_iterations), src_buffer, src_stride_x, src_stride_y,
                    block=block, grid=number_of_blocks)
        # Now color the set
        self.COLOR_KERNEL(numpy.uint(max_iterations),
                          src_buffer, src_stride_x, src_stride_y,
                          driver.Out(output_buffer), dest_stride_x, dest_stride_y,
                          block=block, grid=number_of_blocks)
