#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import os
import numpy
import pyopencl as cl
from . import MandelbrotBase, Precision


FILE_DIR = os.path.abspath(os.path.dirname(__file__))


class Mandelbrot(MandelbrotBase):
    def __init__(self, width, height, device=0, precision=Precision.DOUBLE):
        super(Mandelbrot, self).__init__(width, height, device, precision)
        devices = [self.__get_devices()[device]]
        self.__ctx = cl.Context(devices=devices)
        self.__queue = cl.CommandQueue(self.__ctx)
        with open(os.path.join(FILE_DIR, "kernel.cl"), "rb") as kernel:
            source = kernel.read().decode("UTF-8")
            if Precision(self._precision) != Precision.DOUBLE:
                # No double floating point extension. remove it
                # and set the precision to float
                source = source.replace("#pragma OPENCL EXTENSION cl_khr_fp64: enable", "")
            if "cl_khr_fp64" not in devices[0].extensions and Precision(self._precision) == Precision.DOUBLE:
                raise ValueError("Double Precision calculation is not supported on this device")
            source = source.replace("double", Precision(self._precision).as_ctype())
            self.__program = cl.Program(self.__ctx, source).build()

    @classmethod
    def __get_devices(cls):
        return [dev for platform in cl.get_platforms() for dev in platform.get_devices() if dev.available]

    @classmethod
    def list_devices(cls):
        for i, device in enumerate(cls.__get_devices()):
            print("%d: %s" % (i, device.name.strip()))

    def calculate(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer, gpu_buffer=None):
        dest_stride_x = numpy.int32(output_buffer.strides[0] / output_buffer.dtype.itemsize)
        dest_stride_y = numpy.int32(output_buffer.strides[1] / output_buffer.dtype.itemsize)
        gpu_buffer = cl.Buffer(self.__ctx, cl.mem_flags.WRITE_ONLY, output_buffer.nbytes)

        self.__program.mandelbrot(self.__queue, (self._height, self._width), None,
                                  self._precision(min_x), self._precision(max_x),
                                  self._precision(min_y), self._precision(max_y),
                                  numpy.uint(max_iterations), gpu_buffer, dest_stride_x, dest_stride_y)
        if output_buffer is not None:
            cl.enqueue_copy(self.__queue, output_buffer, gpu_buffer).wait()

    def calculateWithColor(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        src_buffer = cl.Buffer(self.__ctx, cl.mem_flags.READ_WRITE, self._width * self._height * numpy.uint(0).nbytes)
        src_stride_x = numpy.int32(1)
        src_stride_y = numpy.int32(self._height)

        dest_buffer = cl.Buffer(self.__ctx, cl.mem_flags.WRITE_ONLY, output_buffer.nbytes)
        dest_stride_x = numpy.int32(output_buffer.strides[0] / output_buffer.dtype.itemsize)
        dest_stride_y = numpy.int32(output_buffer.strides[1] / output_buffer.dtype.itemsize)

        # First calculate the mandelbrot set
        self.__program.mandelbrot(self.__queue, (self._height, self._width), None,
                                  self._precision(min_x), self._precision(max_x),
                                  self._precision(min_y), self._precision(max_y),
                                  numpy.uint(max_iterations), src_buffer, src_stride_x, src_stride_y)
        # Then color the mandelbrot
        self.__program.colorMandelbrot(self.__queue, (self._height, self._width), None,
                                       numpy.uint(max_iterations),
                                       src_buffer, src_stride_x, src_stride_y,
                                       dest_buffer, dest_stride_x, dest_stride_y)
        # And copy back the output
        cl.enqueue_copy(self.__queue, output_buffer, dest_buffer).wait()
