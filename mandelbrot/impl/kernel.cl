#pragma OPENCL EXTENSION cl_khr_byte_addressable_store: enable
#pragma OPENCL EXTENSION cl_khr_fp64: enable


uint calcMandelbrotPoint(const double c_r, const double c_i, const uint maxIterations) {
    double z_r = 0.0;
    double z_i = 0.0;
    double z_rsqr = z_r * z_r;
    double z_isqr = z_i * z_i;

    for(uint i = 0; i < maxIterations; i++) {
        z_i = z_r * z_i;
        z_i += z_i;
        z_i += c_i;
        z_r = z_rsqr - z_isqr + c_r;
        z_rsqr = z_r * z_r;
        z_isqr = z_i * z_i;
        if(z_rsqr + z_isqr > 4.0) return i;
    }
    return maxIterations;
}

inline uint idx2d(const int x, const int y, const int strideX, const int strideY) {
    return x * strideX + y * strideY;
}

__kernel void mandelbrot(const double min_x, const double max_x, const double min_y, const double max_y,
                         const uint maxIterations, __global uint *buffer, const int strideX, const int strideY) {
    const int height = get_global_size(0);
    const int width = get_global_size(1);
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const uint id = idx2d(i, j, strideX, strideY);

    const double deltaX = (max_x - min_x) / width;
    const double deltaY = (max_y - min_y) / height;
    const double c_r = min_x + deltaX * j;
    const double c_i = min_y + deltaY * i;

    buffer[id] = calcMandelbrotPoint(c_r, c_i, maxIterations);
}

__kernel void colorMandelbrot(const uint maxIterations,
                              __global uint *source, const int src_strideX, const int src_strideY,
                              __global uchar *buffer, const int dest_strideX, const int dest_strideY) {
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    const uint src_id = idx2d(i, j, src_strideX, src_strideY);
    const uint dest_id = idx2d(i, j, dest_strideX, dest_strideY);
    const uint iterations = source[src_id];

    uchar r, g, b;
    if(iterations == maxIterations) {
        r = 0;
        g = 0;
        b = 0;
    } else {
        uint number_of_colors = ceil(pow(maxIterations, 1/3.));
        if(number_of_colors > 256) {
            number_of_colors = 256;
        }
        const uint noc_pow = number_of_colors * number_of_colors;
        const uint color_steps = 256 / number_of_colors;
        if(iterations < number_of_colors) {
            b = iterations * color_steps;
            g = 0;
            r = 0;
        } else if(iterations < noc_pow) {
            b = 255;
            g = (iterations % number_of_colors) * color_steps;
            r = 0;
        } else {
            b = 255;
            g = 255;
            r = (iterations % number_of_colors) * color_steps;
        }
    }
    buffer[dest_id + 0] = r;
    buffer[dest_id + 1] = g;
    buffer[dest_id + 2] = b;
}
