#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import numpy
import math
from functools import partial
from multiprocessing import Pool
from . import MandelbrotBase, Precision


def _iterMandelbrot(max_iterations, c):
    z = 0+0j
    for i in range(max_iterations):
        z = z * z + c
        if abs(z) > 2.0:
            return i
    return max_iterations


def _iterWithColor(max_iterations, c):
    iterations = _iterMandelbrot(max_iterations, c)
    if iterations == max_iterations:
        r, g, b = 0, 0, 0
    else:
        number_of_colors = math.ceil(math.pow(max_iterations, 1/3.))
        if number_of_colors > 256:
            number_of_colors = 256
        color_steps = 256 / number_of_colors
        if iterations < number_of_colors:
            r, g, b = 0, 0, iterations * color_steps
        elif iterations < math.pow(number_of_colors, 2):
            r, g, b = 0, (iterations % number_of_colors) * color_steps, 255
        else:
            r, g, b = (iterations % number_of_colors) * color_steps, 255, 255
    return [r, g, b]


class Mandelbrot(MandelbrotBase):

    def __init__(self, width, height, device=0, precision=Precision.DOUBLE):
        super(Mandelbrot, self).__init__(width, height, device, precision=precision)
        self.__pool = Pool()

    def __del__(self):
        self.__pool.close()
        self.__pool.join()

    @staticmethod
    def list_devices():
        print("0: The local CPU")

    def calculate(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        c_r = numpy.linspace(start=min_x, stop=max_x, num=self._width, dtype=self._precision)
        c_i = numpy.linspace(start=min_y, stop=max_y, num=self._height, dtype=self._precision)
        c = numpy.ravel(c_r +  1j * c_i[:, numpy.newaxis])

        output = self.__pool.map(partial(_iterMandelbrot, max_iterations), c)
        output_buffer[...] = numpy.asarray(output, dtype=output_buffer.dtype).reshape(output_buffer.shape)

    def calculateWithColor(self, min_x, max_x, min_y, max_y, max_iterations, output_buffer):
        c_r = numpy.linspace(start=min_x, stop=max_x, num=self._width, dtype=self._precision)
        c_i = numpy.linspace(start=min_y, stop=max_y, num=self._height, dtype=self._precision)
        c = numpy.ravel(c_r +  1j * c_i[:, numpy.newaxis])

        output = self.__pool.map(partial(_iterWithColor, max_iterations), c)
        output_buffer[...] = numpy.asarray(output, dtype=output_buffer.dtype).reshape(output_buffer.shape)
