#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name="MandelPy",
    version="0.9",
    description="A Qt based interactive viewer of the Mandelbrot set",
    long_description="""
    This package provides an interactive GUI to move around and zoom into the Mandelbrot set.
    It currently features four different backends to choose from: serial, multicore, cuda and opencl.
    """,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: X11 Applications :: Qt5",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Programming Language :: C",
        "Programming Language :: Python :: 3.6"
        "Topic :: Education",
        "Topic :: Scientific/Engineering :: Mathematics",
    ],
    keywords="Mandelbrot Qt5 interactive CUDA OpenCL",

    url="https://bitbucket.org/hansa064/mandelpy",
    author="Torben Hansing",
    author_email="torbenhansing@gmail.com",
    license="MIT",
    zip_safe=False,
    include_package_data=True,

    packages=find_packages("."),
    install_requires=["numpy", "pyQt5"],
    extra_require = {
        "cuda": ["pycuda"],
        "opencl": ["pyopencl"]
    },
    entry_points = {
        "gui_scripts": [
            "mandelpy = mandelbrot.__main__:main"
        ]
    }
)
